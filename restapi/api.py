#from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets, generics
from rest_framework.permissions import AllowAny, IsAdminUser
from reservoir.models import Reservation, Place, Level, Decoration
from rest_framework.response import Response
from django.http import Http404
from rest_framework import serializers
from rest_framework.reverse import reverse

class ReservationSerializator(serializers.ModelSerializer):
    class Meta:
        model = Reservation
        fields = ('pk', 'name', 'email', 'phone', 'confirmed')

class PlaceSerializator(serializers.ModelSerializer):
    class Meta:
        model = Place
        fields = ["x", "y", "id", "state", "table"]



    def state(self, obj):
        return obj.getState()

class LevelSerializator(serializers.ModelSerializer):
    class Meta:
        model = Level
        fields = ("pk", "name")

class DecorationSerializator(serializers.ModelSerializer):
    class Meta:
        model = Decoration
        fields = ("pk", "x", "y", "width", "height", "name")



class LevelView(generics.ListAPIView):
    serializer_class = PlaceSerializator

    def get_permissions(self):
        if(self.request.method == "GET"):
            return [AllowAny()]
        else:
            return [IsAdminUser()]

    def get(self, request, format=None, *args, **kwargs):
        try:
            data = Level.objects.get(pk=self.kwargs["pk"])
        except Level.DoesNotExist as e:
            raise Http404
        level = LevelSerializator(data)

        result = {"level" : level.data}
        if ( request.user.is_authenticated ):
            PlaceSerializator.Meta.fields.append("reservation")
            data = Reservation.objects.all()
            reservations = ReservationSerializator(data, many=True, context={'request' : request})
            result["reservations"] = reservations.data
        pk = self.kwargs["pk"]
        data = self.get_queryset()
        places = PlaceSerializator(data, many=True, context={'request': request})
        result["places"] = places.data
        if(pk is not None):
            data =  Decoration.objects.filter(level__pk = pk)
        else:
            data = Decoration.objects.filter(level__pk = -1)
        result["decorations"] = DecorationSerializator(data, many=True).data
        return Response(result)

    def get_queryset(self):
        queryset = Place.objects.filter(level__pk = -1)
        pk = self.kwargs["pk"]
        if(pk is not None):
            queryset =  Place.objects.filter(level__pk = pk)
        return queryset

class LevelsView(generics.ListAPIView):
    serializer_class = LevelSerializator
    queryset = Level.objects.all()

    def get_permissions(self):
        if(self.request.method == "GET"):
            return [AllowAny()]
        else:
            return [IsAdminUser()]
# router.register(r'users', UserViewSet)
