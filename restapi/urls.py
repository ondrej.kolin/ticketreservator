"""restapi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from restapi.api import LevelView, LevelsView
from reservoir.views import Homepage, ReservationView, ReservationConfirm, ReservationCreate, Reservations, HelpView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^help/', HelpView.as_view(), name="help"),
    url(r'^level/(?P<pk>[0-9]+)', LevelView.as_view()),
    url(r'^level/$', LevelsView.as_view()),
    url(r'^reservations/', Reservations.as_view(), name="reservation_list"),
    url(r'^reservation/create/', ReservationCreate.as_view(), name="create_reservation"),
    url(r'^reservation/show/(?P<pk>[0-9a-zA-Z]+)', ReservationView.as_view(), name="reservation-detail"),
    url(r'^reservation/confirm/(?P<key>[0-9a-zA-Z]+)', ReservationConfirm.as_view(), name="reservation-confirm"),
    url(r'^$', Homepage.as_view(), name="main"),
]
