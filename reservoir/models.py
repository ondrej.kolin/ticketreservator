from django.db import models
from .utils import send_reservation_email

# Create your models here.

class Level(models.Model):
    name = models.CharField(verbose_name="Název:", max_length=254, default="")

    def __str__(self):
        return self.name

class Reservation(models.Model):
    email = models.EmailField(verbose_name="E-mail:", blank=True)
    name = models.CharField(verbose_name="Jméno:", max_length=254, default="")
    phone = models.CharField(verbose_name="Telefonní číslo", max_length=254, default="", blank=True)
    confirmed = models.BooleanField(verbose_name="Potvrzeno?", default=False)
    bar_seats = models.IntegerField(verbose_name="Místa bez čísla: ", default=0)

    def __str__(self):
        return self.name + " - "  + (self.phone if len(self.phone) > 0 else self.email) + " - " + ("potvrzená" if self.confirmed else "NEPOTVRZENÁ")

    def sendmail(self):
        send_reservation_email(self)

    @property
    def places(self):
        places = {}
        for place in self.place_set.all():
            places.setdefault(place.table, 0)
            places[place.table] += 1
        return places

class Place(models.Model):
    x = models.IntegerField(verbose_name="Souřadnice x", default=0)
    y = models.IntegerField(verbose_name="Souřadnice Y", default=0)
    table = models.IntegerField(verbose_name="Číslo stolu", default=-1)
    reservation = models.ForeignKey(Reservation, on_delete=models.SET_NULL, null=True, blank=True)
    level = models.ForeignKey(Level, on_delete=models.CASCADE)

    STATES = {
        "free" : "free",
        "reserved" : "reserved",
        "confirmed": "confirmed"
    }

    def __str__(self):
        return "Place [{}, {}] / Table {} ".format(self.x, self.y, self.table)

    @property
    def state(self):
        if(self.reservation is None):
            return self.STATES["free"]
        elif(self.reservation.confirmed):
            return self.STATES["confirmed"]
        else:
            return self.STATES["reserved"]

    def getState(self):
        if(self.reservation is None):
            return self.STATES["free"]
        elif(self.reservation.confirmed):
            return self.STATES["confirmed"]
        else:
            return self.STATES["reserved"]

    def isFree(self):
        return self.reservation is None

class Decoration(models.Model):
    x = models.IntegerField(verbose_name="Souřadnice x", default=0)
    y = models.IntegerField(verbose_name="Souřadnice y", default=0)
    width = models.IntegerField(verbose_name="Šířka", default=1)
    height = models.IntegerField(verbose_name="Výška", default=1)
    name = models.CharField(verbose_name="Popis", max_length=255)
    level = models.ForeignKey(Level, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.name)
