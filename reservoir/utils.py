# Send reservation

def send_reservation_email(reservation):
    from django.core.mail import EmailMultiAlternatives
    from django.template.loader import get_template
    from django.template import Context
    from django.conf import settings
    
    plaintext = get_template('emails/new_reservation.txt')
    htmly     = get_template('emails/new_reservation.html')
    
    d = { 'reservation': reservation, 'ples_date': settings.PLES_BEGIN, "places":reservation.place_set.all() }
    
    subject, from_email, to = 'Potvrzení rezervace', settings.EMAIL_HOST_USER, reservation.email 
    if to is None or to == "":
        to = from_email
    text_content = plaintext.render(d)
    html_content = htmly.render(d)
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to], bcc=(from_email,))
    msg.attach_alternative(html_content, "text/html")
    msg.send()
	 
