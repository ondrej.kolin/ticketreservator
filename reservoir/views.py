from django.views.generic import TemplateView, ListView, View, DetailView
from django.http import HttpResponse, HttpResponseForbidden, Http404
from django.views.decorators.csrf import csrf_exempt
from reservoir.models import Reservation
from reservoir.models import Place
from django.utils.decorators import method_decorator
from django.contrib.admin.views.decorators import staff_member_required
from django.conf import settings
from django.utils import timezone

class HelpView(TemplateView):
    template_name = "help.html"

class Homepage(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['tickets_begin'] = settings.TICKETS_BEGIN
        context['ples_begin'] = settings.PLES_BEGIN
        print(context)
        return context

	
@method_decorator(staff_member_required, name='dispatch')
class ReservationConfirm(View):
    def get(self, request, key):
        if not(request.user.is_authenticated):
            return HttpResponseForbidden()
        try:
            rezervace = Reservation.objects.get(pk=key)
        except Reservation.DoesNotExist:
            raise Http404
        if (rezervace.confirmed):
            return HttpResponse('{"state":"fail", "data":"Rezervace je již potvrzená", "id":'+str(rezervace.pk)+'}')
        else:
            rezervace.confirmed = True
            rezervace.save()
            return HttpResponse('{"state":"fail", "data":"Rezervace potvrzena", "id":'+str(rezervace.pk)+'}')

	
@method_decorator(staff_member_required, name='dispatch')
class ReservationView(DetailView):
    model = Reservation
    template_name = "reservation.html"

@method_decorator(staff_member_required, name='dispatch')
class Reservations(ListView):
    template_name = "reservations.html"
    model = Reservation

class ReservationCreate(View):
    def get(self, request):
        return HttpResponseForbidden()

    def post(self, request):
        if timezone.now() < settings.TICKETS_BEGIN:
            return HttpResponse('{"state":"fail", "data":"Rezervace ještě nejsou spuštěné"}')
        name = request.POST.get('name')
        email = request.POST.get('email')
        phone = request.POST.get('phone')
        places_no_seat = request.POST.get("bar_seats")
        try:
            if(places_no_seat != ""):
                places_no_seat = int(places_no_seat)
            else:
                places_no_seat = 0
            if(places_no_seat < 0):
                places_no_seat = 0
        except ValueError:
            return HttpResponse('{"state":"fail", "data":"Místa bez čísla stolu nedávají smysl"}')

        if(name == ""):
            return HttpResponse('{"state":"fail", "data":"Jméno není vyplněno"}')
        if(email == "" and phone == "" and not(request.user.is_authenticated)):
            return HttpResponse('{"state":"fail", "data":"Potřebujeme nějaký kontakt, vyplňte prosím e-mail nebo telefon"}')
        if(len(request.POST.getlist('place')) == 0 and places_no_seat < 1):
            return HttpResponse('{"state":"fail", "data":"Nevybral jste žádné místo k rezervování"}')
        rezervace = Reservation.objects.create()
        for misto in request.POST.getlist('place'):
            try:
                misto = Place.objects.get(pk=misto)
            except:
                rezervace.delete()
                return HttpResponse('{"state":"fail"}')
            if not misto.isFree():
                rezervace.delete()
                return HttpResponse('{"state":"fail", "data":"Místo není volné, vyberte prosím jiné.", "reload" : true}')
            rezervace.place_set.add(misto)
        rezervace.email = email
        rezervace.phone = phone
        rezervace.name = name
        rezervace.bar_seats = places_no_seat
        rezervace.save()
        rezervace.sendmail()
        return HttpResponse('{"state":"ok", "pk": "' + str(rezervace.id) + '"}');
