"use strict";

var ROOT_URL = "/level/1/?format=json";
var TICKET_PRICE = 300;
document.querySelector("#price").innerText = TICKET_PRICE;

function reload() {
  chosen_places = getSelectedPlaces()
  var xhr = new XMLHttpRequest();
  xhr.open("GET", ROOT_URL);
  xhr.send(null);
  xhr.onreadystatechange = function () {
    var DONE = 4; // readyState 4 means the request is done.
    var OK = 200; // status 200 is a successful return.
    if (xhr.readyState === DONE) {
      if (xhr.status === OK) loadData(JSON.parse(xhr.responseText)); // 'This is the returned text.'
    } else {
      console.log('Error: ' + xhr.status); // An error occurred during the request.
    }
  };
}
reload()
$("#shopping_button").click(function(e) {
  if (getSelectedPlaces().length == 0) {
    
  }
  e.preventDefault();
  e.stopPropagation()
  $("#shopping").modal("show");
})
$("#reservation-form").submit(function (e) {
  $("#shopping .modal-content").addClass("working")
  e.preventDefault();
  places = getSelectedPlaces();
  document.getElementById("places").innerHTML = "";
  for (var i = 0; i < places.length; ++i) {
    var tmp = document.createElement("input");
    tmp.type = "n";
    tmp.name = "place";
    tmp.value = places[i].getId();
    document.getElementById("places").append(tmp);
  };
  var jqxhr = $.post(this.action, $(this).serialize()).done(function (data) {
    data = JSON.parse(data);
    if (data["state"] == "ok") {
      //Shopovani se podarilo
      $("#shopping_done").toggleClass("show");
      $("#shopping").modal("hide");
      $("#shopping .modal-content").removeClass("working")
      console.log("shopping");
      bookSelectedPlaces();
    } else {
      if (data.hasOwnProperty("reload") && data["data"]) {
	      reload()
              $("#shopping").modal("hide");
	      $("#shopping .modal-content").removeClass("working")
      }
      alert(data["data"] + ". Zkuste chyby napravit, nebo obnovte stránku a zkuste to úplně znovu.");
      $("#shopping .modal-content").removeClass("working")
    }
  }).fail(function () {
    alert("No prosím! To je nepříjemné, někde se stala chyba. Obnovte stránku a pokud se to nezlepší napište nám!")
    $("#shopping .modal-content").removeClass("working")
  });
});
function updatePrice() {
  var pocet = parseInt(document.getElementById("tickets_number").innerText);
  var pocet_bar_seats = parseInt(document.getElementById("inputBarSeats").value);
  document.getElementById("tickets_number_shopping").innerText = "(" + pocet + " + " + pocet_bar_seats + ")";
  document.getElementById("full_price").innerText = (pocet + pocet_bar_seats) * TICKET_PRICE;
}
$('#shopping').on('show.bs.modal', function (e) {
  updatePrice();
});
$("#inputBarSeats").mouseenter(updatePrice)
$("#inputBarSeats").mouseout(updatePrice)
$("#inputBarSeats").bind('keyup mouseup', updatePrice)

$(".modal").click(function(event) {
  event.stopPropagation();
})
