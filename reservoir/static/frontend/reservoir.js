// Hash function
String.prototype.hashCode = function() {
    var hash = 0;
    if (this.length == 0) {
        return hash;
    }
    for (var i = 0; i < this.length; i++) {
        var char = this.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}
// End of hash function

const PLACE_SIZE = 40;
const STATE_FREE = 0;
const STATE_RESERVED = 1;
const STATE_CONFIRMED = 2;
const TICKETS_COUNTER = document.getElementById("tickets_number");
const COLOR_HASHER = new ColorHash()
var COLOR_ARRAY;
var TRANSPARENT;
var NORMAL_STROKE;
var levels = [];
var reservations = {};
var chosen_places = [];
if (admin) {
  var RESERVATION_MASTER = document.getElementById("reservation_master")
}

var mouse = {
  x : undefined,
  y : undefined
};

function getColor(state)
{
  if(state == undefined)
    return;
  return COLOR_ARRAY[state];
}
function Decoration(x, y, width, height, name) {
  var name = name;
  var x = x;
  var y = y;
  var width = width;
  var height = height;

  function _render() {
    fill("white");
    stroke("black");
    rect(x*PLACE_SIZE,y*PLACE_SIZE, width*PLACE_SIZE, height*PLACE_SIZE);
    textFont("sans-serif");
    fill("black");
    stroke("white");
    textSize(PLACE_SIZE*0.7);
    textAlign("center");
    text(name, (x+width/2)*PLACE_SIZE, (y+height/2+0.25)*PLACE_SIZE);
  }

  return {
    getX : () => {return x},
    setX : (x_c) => {x = x_c},
    getY : () => {return y},
    setY : (y_c) => {y = y_c},
    getName : () => {return y},
    setName : (name_c) => {name = name_c},
    getWidth : () => {return width},
    setWidth : (width_c) => {width = width_c},
    getHeight : () => {return height},
    setHeight : (height_c) => {height = height_c},
    render : () => {_render()},
  }

}
function Level() {
  var active = true;
  var places = [];
  var decorations = [];

  function _getClicked() {
    for(let i=0; i<places.length; ++i) {
      if(places[i].mouseOver()) {return places[i]};
    }
    return undefined;
  }

  function _render() {
    for(let i=0; i<decorations.length; ++i) {
      decorations[i].render()
    }
    for(let i=0; i<places.length; ++i) {
      places[i].render()
    }
  }

  function _getSelected() {
    var pl = [];
    for(let i=0;i<places.length;++i) {
      if(places[i].isSelected()) pl.push(places[i]);
    }
    return pl;
  }

  return {
    getActive : () => {return active},
    setActive : (state) => {active = state},
    addPlace : (place) => {places.push(place)},
    createPlace : (id, x, y, state, table, reservation) => {
        let place = new Place(id, x, y, state, table, reservation);
        places.push(place);
        return place;
    },
    createDecoration : (x, y, width, height, name) => {decorations.push(new Decoration(x, y, width, height, name))},
    render : () => {_render()},
    getClicked : () => {return _getClicked()},
    getSelected : () => {return _getSelected()}
  }
}

function Place(id, x, y, state, table, reservation) {
  var id = id;
  if(state == undefined)
    state = 0;
  switch(state)
  {
    case "reserved":
      state = STATE_RESERVED;
      break;
    case "confirmed":
      state = STATE_CONFIRMED;
      break;
    default:
      state = STATE_FREE
  }
  var x = x;
  var y = y;
  var selected = false;
  var table = table;
  var reservation = reservation


  function isHovered() {
    return mouse.x > x*PLACE_SIZE && mouse.x < (x+1)*PLACE_SIZE &&
    mouse.y > y*PLACE_SIZE && mouse.y < (y+1)*PLACE_SIZE
  }

  function _render() {
    stroke("black");
    fill(getColor(state));
    rect(x*PLACE_SIZE,y*PLACE_SIZE, PLACE_SIZE, PLACE_SIZE);
    fill(color(255,255,255));
    if(isHovered()) {
      fill(TRANSPARENT);
      noStroke();
      rect(x*PLACE_SIZE,y*PLACE_SIZE, PLACE_SIZE, PLACE_SIZE);
      stroke(NORMAL_STROKE);
    }
    textFont("sans-serif");
    textSize(PLACE_SIZE*0.55);
    fill("black");
    noStroke();
    text(table, x*PLACE_SIZE+PLACE_SIZE*0.5, (y+1)*PLACE_SIZE-PLACE_SIZE*0.3);
    textAlign("center");
    if( admin ) {
      clr = _getColor();
      if (clr != null) {
        fill(color(clr.r, clr.g, clr.b));
        rect(x*PLACE_SIZE,(y+0.75)*PLACE_SIZE, PLACE_SIZE, PLACE_SIZE/4);
      }
    }
    if( selected ) {
      textFont(fontawesome);
      textSize(PLACE_SIZE*0.75);
      fill(color(0,0,0));
      text("\uf058", x*PLACE_SIZE+PLACE_SIZE*0.5, (y+1)*PLACE_SIZE-PLACE_SIZE*0.22);
    }

  }

  function _getColor () {
    if (reservation == null) {
      return null;
    }
    if (reservation.color != undefined) {
      return reservation.color
    }
    seed = reservation.email.hashCode() + reservation.phone.hashCode() + reservation.name.hashCode()
    let color = COLOR_HASHER.rgb(seed)
    reservation.color = {
      r : color[0],
      g : color[1],
      b : color[2]
    }
    return reservation.color
  }

  return {
    getId : () => {return id},
    getState : () => {return state},
    setState : (state_a) => {state = state_a},
    getColor : () => { return _getColor()},
    getX : () => {return x},
    setX : (x_c) => {x = x_c},
    getY : () => {return y},
    setY : (y_c) => {y = y_c},
    render : () => {_render()},
    mouseOver : () => {return isHovered()},
    toggleSelection : () => {if(state == STATE_FREE) selected = !selected},
    isSelected : () => {return selected},
    clearSelection: () => { selected = false}
  }
}
var level;
var fontawesome;
var canvas;

function preload() {
  fontawesome = loadFont('/static/frontend/assets/FontAwesome.otf');
}

function fixCanvas() {
  canvas.resize(canvas.parent().offsetWidth, canvas.parent().offsetHeight);
}

function windowResized() {
  fixCanvas();
}

function setup() {
  canvas = createCanvas(16*PLACE_SIZE, (40*PLACE_SIZE)+1);
  canvas.parent("playground");
  COLOR_ARRAY = [color("#28a745"), color("#ffc107"), color("#dc3545")];
  TRANSPARENT = color('rgba(255, 255, 255, 0.7)');
  NORMAL_STROKE = color(0, 0, 0);
  fixCanvas();
}

function loadData(level_data)
{
  levels = [];
  reservations = {};
  TICKETS_COUNTER.innerText = "0"
  booked_ids = [] 
  for(let i=0; i<chosen_places.length; ++i) {
    booked_ids.push(chosen_places[i].getId())
  }
  level = new Level();
  places = level_data["places"];
  decorations = level_data["decorations"];
  if (admin) {
    level_data["reservations"].forEach(function (item) {
                                          reservations[item.pk] = item
                                       })
  }
  level_label = document.querySelector("#level");
  if(level_label != undefined)
    level_label.innerText = level_data["level"].name
  for(let i=0; i<places.length; ++i)
  {
    place = places[i];
    reservation = null;
    if (admin && place.reservation) {
      reservation = reservations[place.reservation]
      if (reservation.places == undefined) {
        reservation.places = [level.createPlace(place.id, place.x, place.y, place.state, place.table, reservation)]
      } else {
        reservation.places.push(level.createPlace(place.id, place.x, place.y, place.state, place.table, reservation))
      }
      if (reservation.html == undefined) {
        reservation.html = createReservationEntry(reservation)
        RESERVATION_MASTER.appendChild(reservation.html);
      }
    }
    else {
      place = level.createPlace(place.id, place.x, place.y, place.state, place.table, reservation)
      if(booked_ids.includes(place.getId())) {
        place.toggleSelection() 
      }
    }
  }
  for(let i=0; i<decorations.length; ++i)
  {
    decoration = decorations[i];
    level.createDecoration(decoration.x, decoration.y, decoration.width, decoration.height, decoration.name)
  }
  levels.push(level);
}

function mouseClicked() {
  let selectedPlace = level.getClicked();
  if(selectedPlace == undefined)
    return;
  selectedPlace.toggleSelection();

}

function draw() {
  mouse.x = mouseX;
  mouse.y = mouseY;
  for(let i=0;i<levels.length; ++i)
  {
    levels[i].render();
  }
  updatePanel();
}

function updatePanel() {
  var pocet = 0;
  for(let i=0; i<levels.length; ++i) {
    pocet += levels[i].getSelected().length;
  }
    TICKETS_COUNTER.innerText = pocet;
  
}

function drawSidePanel() {

}

function getSelectedPlaces() {
  var places = [];
  for(let i=0;i<levels.length; ++i)
  {
    places = places.concat(levels[i].getSelected());
  }
  return places;
}

function bookSelectedPlaces() {
  let places = getSelectedPlaces();
  for(let i=0; i<places.length; ++i)
  {
    places[i].setState(STATE_RESERVED);
    places[i].clearSelection();
  }
}

function dectohex (number) {
  hexString = number.toString(16);
  if (hexString.length % 2) {
    hexString = '0' + hexString;
  }
  return hexString;
}

function createReservationEntry (reservation) {
  let confirmed_text;
  if (reservation.confirmed) {
    confirmed_text = "Potvrzeno"
  }
  else {
    confirmed_text = `<a href="/reservation/confirm/${reservation.pk}"><i class="fa fa-check"></i> Potvrdit</a>`
  }
  let clr = reservation.places[0].getColor()
  let color_string = "#" + dectohex(clr.r) + dectohex(clr.g) + dectohex(clr.b)
  wrapper = document.createElement("div")
  wrapper.id = "reservation" + reservation.pk
  wrapper.style.borderBottomColor = color_string
  wrapper.className = "col-sm-3 col-xs-6 reservation"
  wrapper.innerHTML = `<i class="fa fa-user" style="color: ${color_string}"></i> ${reservation.name}, ${confirmed_text}`
  wrapper.innerHTML += `, <a href="/reservation/show/${reservation.pk}">Detail <i class="fa fa-search-plus"></i></a>`
  return wrapper;
}
